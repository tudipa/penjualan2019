@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-head container-fluid" style="margin-top: 10px;">
                    <p>Data Detail produk</p>
                </div>
                <div class="form-horizontal">
                    <div class="panel-body">
                        
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group">
                            <label class="col-sm-2">Nama Produk</label>
                            <div class="col-sm-10">
                                <p>{{ "Nama Produk" }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Kategori Produk</label>
                            <div class="col-sm-10">
                                {{ "Kategori Produk" }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Qty Awal</label>
                            <div class="col-sm-10">
                                <p>{{ "Jumlah" }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Harga Jual</label>
                            <div class="col-sm-10">
                                <p>{{ "Harga jual" }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2">Harga Beli</label>
                            <div class="col-sm-10">
                                <p>{{ "Harga beli" }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a href="{{ route('produk.index') }}" class="btn btn-warning">Data Produk</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
