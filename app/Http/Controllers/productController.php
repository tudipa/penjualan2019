<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\produk;

class productController extends Controller
{
    
    public function dataPelanggan()
    {
        $pelanggan = ['Ina','Ani','Ita','Indra'];
        return $pelanggan;
    }

    
    public function indexPelanggan()
    {
        $pelanggan = $this->dataPelanggan();
        return view('pelanggan/index',compact('pelanggan'));
    }

    public function lihat()
    {
        $produk = DB::table('produks')->get();
    	return view('produk/index',compact('produk'));
    }

    public function lihatjoin()
    {
        $produk = DB::table('produks')
                    ->join('kategori','produks.id_kategori','=','kategori.id')
                    ->get();
                    
        return view('produk/indexjoin',compact('produk'));
    }


    public function tambah()
    {
        DB::table('produks')
        ->insert([
            'nama' => 'Lampu',
            'id_kategori' => 1,
            'qty' => 14,
            'harga_beli' => 40000,
            'harga_jual' => 60000,
        ]);

        echo "Data Berhasil Ditambah";
    }

    public function ubah()
    {
        DB::table('produks')->where('id',5)
        ->update([
            'nama' => 'Monitor',
            'qty' => 20,
            'harga_beli' => 45000,
            'harga_jual' => 55000,
        ]);

        echo "Data Berhasil Diperbaharui";
    }

    public function hapus()
    {
        DB::table('produks')->where('id',5)->delete();
        echo "Data Berhasil Dihapus";
    }
    
    public function create()
    {
        $kategori = \App\kategori::all();
        return view('prod2.create',compact('kategori'));
    }

    public function store(Request $request)
    {

        $aturan = [
            'nama' => 'required',
            'kategori' => 'required',
            'qty' => 'required|numeric',
            'beli' => 'required|numeric',
            'jual' => 'required|numeric',
        ];

        $pesan = [
        'required' => 'Data ini Wajib Diisi!',
        'numeric' => 'Mohon isi dengan angka'
        ];

        $this->validate($request,$aturan,$pesan);


        produk::create([
            'nama' => $request->nama,
            'id_kategori' => $request->kategori,
            'qty' => $request->qty,
            'harga_beli' => $request->beli,
            'harga_jual' => $request->jual,
        ]);
        return redirect()->route('produk.index');
    }

    public function index()
    {
        $produk = produk::all();
        return view('prod2.index', compact('produk'));
    }

    public function show($id)
    {
        $produk = produk::where('id',$id)->first();
        return view('prod2.show',compact('produk'));
    }

    public function edit($id)
    {
        $produk = produk::where('id',$id)->first();
        $kategori = \App\kategori::all();
        return view('prod2.edit',compact('produk', 'kategori'));
    }


    public function update(Request $request, $id)
    {
        produk::where('id',$id)
        ->update([
            'nama' => $request->nama,
            'id_kategori' => $request->kategori,
            'qty' => $request->qty,
            'harga_beli' => $request->beli,
            'harga_jual' => $request->jual,
        ]);
        return redirect()->route('produk.index');
    }

    public function destroy($id)
    {
        produk::where('id',$id)->delete();
        return redirect()->route('produk.index');
    }
}
